import javax.swing.*;
import java.awt.*;

public class SettingPanel extends JPanel {
    private final JButton addButton;
    private final JButton delButton;
    private final JButton resetButton;


    public SettingPanel(){
        this.addButton = new JButton("Ajout Rectangle");
        this.delButton = new JButton("Suppr Rectangle");
        this.resetButton = new JButton("Reset Rectangles");
        setLayout(new GridLayout(3,1));
        add(this.addButton);
        add(this.delButton);
        add(this.resetButton);

    }

    public JButton getAddButton() {
        return addButton;
    }

    public JButton getDelButton() {
        return delButton;
    }

    public JButton getResetButton() {
        return resetButton;
    }
}
