import java.awt.*;

/**
 *
 */
public class Rectangle implements Runnable{
    private final Panneau panneau;
    private int x;
    private int y;
    private int xSpeed;
    private int ySpeed;
    private int height;
    private int width;
    private Color color;
    private int panelWidth;
    private int panelHeight;
    private boolean wait = false;

    public Rectangle(Panneau p, int xOrigin, int yOrigin, int xSpeed, int ySpeed, int height, int width, Color color, int panelWidth, int panelHeight) {
        this.panneau = p;
        this.x = xOrigin;
        this.y = yOrigin;
        this.xSpeed = xSpeed;
        this.ySpeed = ySpeed;
        this.height = height;
        this.width = width;
        this.color = color;
        this.panelWidth = panelWidth;
        this.panelHeight = panelHeight;


    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getxSpeed() {
        return xSpeed;
    }

    public void setxSpeed(int xSpeed) {
        this.xSpeed = xSpeed;
    }

    public int getySpeed() {
        return ySpeed;
    }

    public void setySpeed(int ySpeed) {
        this.ySpeed = ySpeed;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }


    @Override
    public void run() {
        while(true) {
            if (x <= 0 || x + width >= panelWidth) {
                xSpeed *= -1;
            }

            if (y <= 0 || y + height >= panelHeight) {
                ySpeed *= -1;
            }
            x += xSpeed;
            y += ySpeed;

            Rectangle otherRect  = panneau.collision(this);
            if(otherRect != null) {
                System.out.println("Collision entre " + this + " et " + otherRect + " : "+ (this.wait ? "waiting" : "pas waiting"));
                if(this.wait && otherRect.isWait()){
                    System.out.println("Notify");
                    panneau.notify();
                    this.flip();
                    this.wait = false;
                    otherRect.setWait(false);
                } else {
                    this.wait = true;
                    otherRect.setWait(true);
                    try {
                        System.out.println("Wait");
                        panneau.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

            }
            try {
                Thread.sleep(20);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void flip(){
        xSpeed *= -1;
        ySpeed *= -1;
        x += xSpeed;
        y += ySpeed;
    }

    public void setWait(boolean b){
        this.wait = b;
    }

    public boolean isWait(){return wait;}

}
