import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
* @author CEDSI n NAVET56
*/
public class Main {
 public static void main(String[] args) {
   testComponent("Thread game",new Panneau(900,900)) ;
 }

 public static final void testComponent (final String title, Panneau component) {
     SwingUtilities.invokeLater(() -> {
         JFrame jFrame = new JFrame(title);
         jFrame.addWindowListener(new WindowAdapter() {
                public void windowClosing(WindowEvent e) {
                    System.exit(0);
                }
            });
         jFrame.getContentPane().add (component, BorderLayout.CENTER);
         SettingPanel settingPanel = new SettingPanel();
         settingPanel.getAddButton().addActionListener( e -> component.addRect());
         settingPanel.getDelButton().addActionListener( e -> component.delRect());
         settingPanel.getResetButton().addActionListener( e -> component.resetRect());

         jFrame.add(settingPanel, BorderLayout.EAST);

         jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
         jFrame.pack();
         Dimension screenSize = Toolkit.getDefaultToolkit ().getScreenSize ();
         Dimension size = jFrame.getSize();
         jFrame.setLocation ((screenSize.width - size.width)/4, (screenSize.height - size.height)/4);
         jFrame.setVisible(true);
         new Thread(component).start();

      });
    }
}
