
import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

public class Panneau extends JPanel implements Runnable{

  private final int width;
  private final int height;

  private final int defaultHeight = 10;
  private final int defaultWidth = 70;

  private ArrayList<Rectangle> rects;
  private ArrayList<Thread> tasks;

  public Panneau(int width, int height) {
    this.width = width;
    this.height = height;
    this.tasks = new ArrayList<>();
    Dimension size = new Dimension(width, height);
    setMinimumSize (size);
    setPreferredSize (size);
    setDoubleBuffered (true);
    this.rects = new ArrayList<>();
    resetRect();
  }

  public Rectangle collision(Rectangle r1) {
    int x1, x2, y1, y2, x1max, x2max, y1max, y2max;
    for (Rectangle r2 : rects) {
      if (r1 != r2) {
        x1 = r1.getX();
        x2 = r2.getX();
        y1 = r1.getY();
        y2 = r2.getY();
        y1max = y1 + defaultHeight;
        x1max = x1 + defaultWidth;

        boolean xCollision = ((x1 < x2) && (x1max > x2)) || ((x1 > x2) && ((x1 - defaultWidth) < x2));

        boolean yCollision = (y1 < y2 && y1max > y2) || (y1 > y2 && y1 - defaultHeight < y2);

        if (xCollision && yCollision) {
          return r2;
        }

      }
    }
      return null;

  }

  public synchronized void update () {
    repaint ();
  }

  public void paint (Graphics g) {
    Toolkit.getDefaultToolkit().sync();
    Graphics2D g2 = (Graphics2D) g;
    g2.setBackground (Color.WHITE);
    g2.clearRect (0, 0, width, height);

    for (Rectangle rect : rects) {
      g2.setColor(rect.getColor());
      g2.fillRect(rect.getX(), rect.getY(), rect.getWidth(), rect.getHeight());

    }

  }


  @Override
  public void run () {
    while(true){
      update();
    }
  }

  public void addRect() {
    this.rects.add(new Rectangle(this,(int)(Math.random() * height-30), (int)(Math.random()*width-50), (int)(Math.random() * 7), (int)(Math.random() * 7), defaultHeight, defaultWidth, getRandomColor(), width, height));
    this.tasks.add(new Thread(this.rects.get(this.rects.size()-1), ""+(this.tasks.size()-1)));
    this.tasks.get(this.tasks.size()-1).start();

  }

  public void resetRect(){
    this.rects.clear();
    this.rects.add(new Rectangle(this,10, 20, 3, 0, defaultHeight, defaultWidth, Color.GREEN, width, height));
    this.tasks.add(new Thread(this.rects.get(this.rects.size()-1)));
    this.tasks.get(this.tasks.size()-1).start();
    this.rects.add(new Rectangle(this,100, 50, 0, 1, defaultHeight, defaultWidth, Color.MAGENTA, width, height));
    this.tasks.add(new Thread(this.rects.get(this.rects.size()-1)));
    this.tasks.get(this.tasks.size()-1).start();
    this.rects.add(new Rectangle(this,0, 350, 0, 1, defaultHeight, defaultWidth, Color.BLACK, width, height));
    this.tasks.add(new Thread(this.rects.get(this.rects.size()-1)));
    this.tasks.get(this.tasks.size()-1).start();
    this.rects.add(new Rectangle(this,20, 250, 1, 6, defaultHeight, defaultWidth, Color.BLUE, width, height));
    this.tasks.add(new Thread(this.rects.get(this.rects.size()-1)));
    this.tasks.get(this.tasks.size()-1).start();
  }

  private Color getRandomColor(){
    Color color;
    switch ((int) (Math.random() * 8)){
      case 1:
        color = Color.BLUE;
        break;
      case 2:
        color = Color.BLACK;
        break;
      case 3:
        color = Color.MAGENTA;
        break;
      case 4:
        color = Color.CYAN;
        break;
      case 5:
        color = Color.ORANGE;
        break;
      case 6:
        color = Color.YELLOW;
        break;
      case 7:
        color = Color.PINK;
        break;
      case 8:
        color = Color.RED;
        break;
      default:
        color = Color.GREEN;
    }
    return color;
  }

  public void delRect() {
    this.rects.remove(this.rects.get(this.rects.size()-1));

  }

}
